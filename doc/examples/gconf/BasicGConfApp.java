package gconf;

import org.gnu.gconf.ConfClient;
import org.gnu.gconf.ConfClientListener;
import org.gnu.gconf.ConfClientPreloadType;
import org.gnu.gconf.ConfEntry;
import org.gnu.gconf.ConfException;
import org.gnu.gtk.*;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class BasicGConfApp {
    private ConfClient client;

    private Window win;

    private Label fooLabel;

    private Label blahLabel;

    private Label bazLabel;

    private Label barLabel;

    private final String fooKey = "/apps/basic-gconf-app/foo";

    private final String barKey = "/apps/basic-gconf-app/bar";

    private final String bazKey = "/apps/basic-gconf-app/baz";

    private final String blahKey = "/apps/basic-gconf-app/blah";

    public BasicGConfApp() {
        // get the default client.
        client = ConfClient.getInstance();

        // Tell ConfClient that we are interested in the given directory.
        // This means ConfClient will receive notifications of changes to
        // this directory, and cache keys under this directory.
        try {
            client.addDirectory("/apps/basic-gconf-app",
                    ConfClientPreloadType.NONE);
        } catch (ConfException e) {
            System.err.println("Error adding directory to ConfClient");
            Gtk.mainQuit();
        }

        createMainWindow();

        win.showAll();
    }

    private void createMainWindow() {
        win = new Window(WindowType.TOPLEVEL);
        win.setTitle("BasicGConfApp Main Window");
        win.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DELETE)
                        || event.isOfType(LifeCycleEvent.Type.DESTROY))
                    Gtk.mainQuit();
                return false;
            }
        });

        VBox vbox = new VBox(false, 5);
        win.add(vbox);
        vbox.setBorderWidth(5);
        fooLabel = new Label("");
        Frame frame = createConfigurableLabel(new ConfClientListener() {
            public void clientNotify(ConfEntry entry) {
                String str = entry.getValue().getString();
                if (str != null)
                    fooLabel.setText(str);
            }
        }, fooKey, fooLabel);
        vbox.packStart(frame, true, true, 0);

        barLabel = new Label("");
        frame = createConfigurableLabel(new ConfClientListener() {
            public void clientNotify(ConfEntry entry) {
                String str = entry.getValue().getString();
                if (str != null)
                    barLabel.setText(str);
            }
        }, barKey, barLabel);
        vbox.packStart(frame, true, true, 0);

        bazLabel = new Label("");
        frame = createConfigurableLabel(new ConfClientListener() {
            public void clientNotify(ConfEntry entry) {
                String str = entry.getValue().getString();
                if (str != null)
                    bazLabel.setText(str);
            }
        }, bazKey, bazLabel);
        vbox.packStart(frame, true, true, 0);

        blahLabel = new Label("");
        frame = createConfigurableLabel(new ConfClientListener() {
            public void clientNotify(ConfEntry entry) {
                String str = entry.getValue().getString();
                if (str != null)
                    blahLabel.setText(str);
            }
        }, blahKey, blahLabel);
        vbox.packStart(frame, true, true, 0);

        Button prefs = new Button("_Prefs", true);
        vbox.packEnd(prefs, false, false, 0);
        prefs.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK))
                    prefsClicked();
            }
        });
    }

    private Frame createConfigurableLabel(ConfClientListener list,
            String configKey, Label label) {
        Frame frame = new Frame("Value of " + configKey);
        frame.add(label);
        try {
            String str = client.getString(configKey);
            if (str != null)
                label.setText(str);
            client.addListener(list, configKey);
        } catch (ConfException e) {

        }
        return frame;
    }

    private void prefsClicked() {
        Dialog dialog = new Dialog();
        dialog.setTitle("Preferences");
        Button cancel = new Button(GtkStockItem.CANCEL);
        dialog.addWidget(cancel, ResponseType.CANCEL.getValue());
        Button apply = new Button(GtkStockItem.APPLY);
        dialog.addWidget(apply, ResponseType.APPLY.getValue());
        VBox vbox = new VBox(false, 5);
        vbox.setBorderWidth(5);
        dialog.getDialogLayout().packStart(vbox, true, true, 0);

        final Entry fooEntry = new Entry();
        HBox hbox = createConfigEntry(fooKey, fooEntry);
        vbox.packStart(hbox, false, false, 0);

        final Entry barEntry = new Entry();
        hbox = createConfigEntry(barKey, barEntry);
        vbox.packStart(hbox, false, false, 0);

        final Entry bazEntry = new Entry();
        hbox = createConfigEntry(bazKey, bazEntry);
        vbox.packStart(hbox, false, false, 0);

        final Entry blahEntry = new Entry();
        hbox = createConfigEntry(blahKey, blahEntry);
        vbox.packStart(hbox, false, false, 0);

        apply.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.getType() == ButtonEvent.Type.CLICK) {
                    configEntryCommit(fooEntry, fooKey);
                    configEntryCommit(barEntry, barKey);
                    configEntryCommit(bazEntry, bazKey);
                    configEntryCommit(blahEntry, blahKey);
                }
            }
        });
        dialog.showAll();
        int response = dialog.run();
        dialog.destroy();
    }

    private HBox createConfigEntry(String key, Entry entry) {
        HBox hbox = new HBox(false, 5);
        Label label = new Label(key);
        entry.setData("key", key);
        hbox.packStart(label, false, false, 0);
        hbox.packStart(entry, false, false, 0);
        try {
            String str = client.getString(key);
            if (null != str) {
                entry.setText(str);
            }
        } catch (ConfException e) {

        }

        return hbox;
    }

    private void configEntryCommit(Entry entry, String key) {
        String text = entry.getCharacters(0, -1);
        try {
            if (null != text)
                client.setString(key, text);
            else
                client.unset(key);
        } catch (ConfException e) {

        }
    }

    public static void main(String[] args) {
        Gtk.init(null);
        BasicGConfApp app = new BasicGConfApp();

        Gtk.main();
    }
}
