/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gconf/gconf.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gconf_ConfMetaInfo.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1new
  (JNIEnv *env, jclass cls)
{
	return getStructHandle(env, gconf_meta_info_new(), NULL,
			(JGFreeFunc) gconf_meta_info_free);
}

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1free
  (JNIEnv *env, jclass cls, jobject mi)
{
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	gconf_meta_info_free(mi_g);
}

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_get_schema
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1get_1schema
  (JNIEnv *env, jclass cls, jobject mi)
{
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	return (*env)->NewStringUTF(env, gconf_meta_info_get_schema(mi_g));
}

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_get_mod_user
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1get_1mod_1user
  (JNIEnv *env, jclass cls, jobject mi)
{
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	return (*env)->NewStringUTF(env, gconf_meta_info_get_mod_user(mi_g));
}

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_get_mod_time
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1get_1mod_1time
  (JNIEnv *env, jclass cls, jobject mi)
{
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	return (jlong)gconf_meta_info_mod_time(mi_g);
}

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_set_schema
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1set_1schema
  (JNIEnv *env, jclass cls, jobject mi, jstring schema)
{
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, schema, NULL);
	gconf_meta_info_set_schema(mi_g, str);
	(*env)->ReleaseStringUTFChars(env, schema, str);
}

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_set_mod_user
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1set_1mod_1user
  (JNIEnv *env, jclass cls, jobject mi, jstring user)
 {
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, user, NULL);
	gconf_meta_info_set_mod_user(mi_g, str);
	(*env)->ReleaseStringUTFChars(env, user, str);
 }

/*
 * Class:     org_gnu_gconf_ConfMetaInfo
 * Method:    gconf_meta_info_set_mod_time
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfMetaInfo_gconf_1meta_1info_1set_1mod_1time
  (JNIEnv *env, jclass cls, jobject mi, jlong time)
{
	GConfMetaInfo* mi_g = (GConfMetaInfo*)getPointerFromHandle(env, mi);
	gconf_meta_info_set_mod_time(mi_g, (GTime)time);
}

#ifdef __cplusplus
}
#endif
