/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gconf/gconf.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gconf_ConfSchema.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1new
  (JNIEnv *env, jclass cls)
{
	return getStructHandle(env, gconf_schema_new(), NULL,
			(JGFreeFunc) gconf_schema_free);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1free
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gconf_schema_free(schema_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1type
  (JNIEnv *env, jclass cls, jobject schema, jint type)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gconf_schema_set_type(schema_g, (GConfValueType)type);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_list_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1list_1type
  (JNIEnv *env, jclass cls, jobject schema, jint type)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gconf_schema_set_list_type(schema_g, (GConfValueType)type);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_car_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1car_1type
  (JNIEnv *env, jclass cls, jobject schema, jint type)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gconf_schema_set_car_type(schema_g, (GConfValueType)type);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_cdr_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1cdr_1type
  (JNIEnv *env, jclass cls, jobject schema, jint type)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gconf_schema_set_cdr_type(schema_g, (GConfValueType)type);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_locale
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1locale
  (JNIEnv *env, jclass cls, jobject schema, jstring locale)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gchar* str = (gchar*)(*env)->GetStringUTFChars(env, locale, NULL);
	gconf_schema_set_locale(schema_g, str);
	(*env)->ReleaseStringUTFChars(env, locale, str);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_short_desc
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1short_1desc
  (JNIEnv *env, jclass cls, jobject schema, jstring desc)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gchar* str = (gchar*)(*env)->GetStringUTFChars(env, desc, NULL);
	gconf_schema_set_short_desc(schema_g, str);
	(*env)->ReleaseStringUTFChars(env, desc, str);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_long_desc
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1long_1desc
  (JNIEnv *env, jclass cls, jobject schema, jstring desc)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gchar* str = (gchar*)(*env)->GetStringUTFChars(env, desc, NULL);
	gconf_schema_set_long_desc(schema_g, str);
	(*env)->ReleaseStringUTFChars(env, desc, str);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_owner
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1owner
  (JNIEnv *env, jclass cls, jobject schema, jstring owner)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gchar* str = (gchar*)(*env)->GetStringUTFChars(env, owner, NULL);
	gconf_schema_set_owner(schema_g, str);
	(*env)->ReleaseStringUTFChars(env, owner, str);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_default_value
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1default_1value
  (JNIEnv *env, jclass cls, jobject schema, jobject value)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_schema_set_default_value(schema_g, value_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_set_default_value_nocopy
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1set_1default_1value_1nocopy
  (JNIEnv *env, jclass cls, jobject schema, jobject value)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_schema_set_default_value_nocopy(schema_g, value_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1type
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	return (jint)gconf_schema_get_type(schema_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_list_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1list_1type
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	return (jint)gconf_schema_get_list_type(schema_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_car_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1car_1type
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	return (jint)gconf_schema_get_car_type(schema_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_cdr_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1cdr_1type
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	return (jint)gconf_schema_get_cdr_type(schema_g);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_locale
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1locale
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	const char* locale = gconf_schema_get_locale(schema_g);
	return (*env)->NewStringUTF(env, locale);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_short_desc
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1short_1desc
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	const char* desc = gconf_schema_get_short_desc(schema_g);
	return (*env)->NewStringUTF(env, desc);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_long_desc
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1long_1desc
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	const char* desc = gconf_schema_get_long_desc(schema_g);
	return (*env)->NewStringUTF(env, desc);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_owner
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1owner
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	const char* owner = gconf_schema_get_owner(schema_g);
	return (*env)->NewStringUTF(env, owner);
}

/*
 * Class:     org_gnu_gconf_ConfSchema
 * Method:    gconf_schema_get_default_value
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfSchema_gconf_1schema_1get_1default_1value
  (JNIEnv *env, jclass cls, jobject schema)
{
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	return getStructHandle(env, gconf_schema_get_default_value(schema_g),
			(JGCopyFunc) gconf_value_copy, (JGFreeFunc) gconf_value_free);
}

#ifdef __cplusplus
}
#endif
