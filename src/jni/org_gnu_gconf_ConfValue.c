/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gconf/gconf.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gconf_ConfValue.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1new
  (JNIEnv *env, jclass cls, jint type)
{
	return getStructHandle(env, gconf_value_new((GConfValueType) type), NULL,
			(JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1copy
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return getStructHandle(env, gconf_value_copy(value_g), NULL,
			(JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1free
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_value_free(value_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1string
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gchar* result = (gchar*)gconf_value_get_string(value_g);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_int
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1int
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return (jint)gconf_value_get_int(value_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_float
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1float
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return (jdouble)gconf_value_get_float(value_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_list_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1list_1type
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return (jint)gconf_value_get_list_type(value_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_list
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1list
  (JNIEnv *env, jclass cls, jobject value)
{
	GSList* it;
	GConfValue* value_g;
	GSList* list;
	int i;
	jclass aCls;
	jobjectArray ar;
	jobject schema;
	GConfValueType vt;
	value_g = (GConfValue*)getPointerFromHandle(env, value);
	list = gconf_value_get_list(value_g);
	i = 0;
	aCls = NULL;
	schema = NULL;
	vt = gconf_value_get_list_type(value_g);
	if (vt == GCONF_VALUE_STRING)
		aCls = (*env)->FindClass(env, "java/lang/String");
	else if (vt == GCONF_VALUE_INT)
		aCls = (*env)->FindClass(env, "java/lang/Integer");
	else if (vt == GCONF_VALUE_FLOAT)
		aCls = (*env)->FindClass(env, "java/lang/Double");
	else if (vt == GCONF_VALUE_BOOL)
		aCls = (*env)->FindClass(env, "java/lang/Boolean");
	else if (vt == GCONF_VALUE_SCHEMA)
		aCls = (*env)->FindClass(env, "org/gnu/gconf/ConfSchema");
	if (NULL == aCls)
		return NULL;
	ar = (*env)->NewObjectArray(env, g_slist_length(list), aCls, NULL);
	for (it = list; it != NULL; it = it->next, ++i)	{
		if (vt == GCONF_VALUE_STRING) {
			jstring str = (*env)->NewStringUTF(env, it->data);
			(*env)->SetObjectArrayElement(env, ar, i, str);
		} else if (vt == GCONF_VALUE_INT) {
			jmethodID mid = (*env)->GetMethodID(env, aCls, "<init>", "(I)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			obj = (*env)->NewObject(env, aCls, mid, (jint)GPOINTER_TO_INT(it->data));
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		} else if (vt == GCONF_VALUE_FLOAT) {
			jmethodID mid = (*env)->GetMethodID(env, aCls, "<init>", "(D)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			obj = (*env)->NewObject(env, aCls, mid, (jdouble*)it->data);
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		} else if (vt == GCONF_VALUE_BOOL) {
			jmethodID mid = (*env)->GetMethodID(env, aCls, "<init>", "(Z)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			obj = (*env)->NewObject(env, aCls, mid, (jboolean)(GPOINTER_TO_INT(it->data) != 0));
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		} else if (vt == GCONF_VALUE_SCHEMA) {
			jmethodID mid = (*env)->GetStaticMethodID(env, aCls, "getConfSchema", "(I)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			
			schema = getStructHandle(env, it->data,
					(JGCopyFunc) gconf_schema_copy, (JGFreeFunc) gconf_schema_free);
			obj = (*env)->CallStaticObjectMethod(env, aCls, mid, schema);
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		}
	}
	return ar;
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_car
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1car
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return getStructHandle(env, gconf_value_get_car(value_g),
		(JGCopyFunc) gconf_value_copy, (JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_cdr
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1cdr
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return getStructHandle(env, gconf_value_get_cdr(value_g),
			(JGCopyFunc) gconf_value_copy, (JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_bool
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1bool
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return (jboolean)gconf_value_get_bool(value_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_get_schema
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1get_1schema
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	return getStructHandle(env, gconf_value_get_schema(value_g),
			(JGCopyFunc) gconf_schema_copy, (JGFreeFunc) gconf_schema_free);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_int
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1int
  (JNIEnv *env, jclass cls, jobject value, jint theInt)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_value_set_int(value_g, (int)theInt);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1string
  (JNIEnv *env, jclass cls, jobject value, jstring theString)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	const gchar* str = (*env)->GetStringUTFChars(env, theString, NULL);
	gconf_value_set_string(value_g, str);
	(*env)->ReleaseStringUTFChars(env, theString, str);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_bool
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1bool
  (JNIEnv *env, jclass cls, jobject value, jboolean theBool)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_value_set_bool(value_g, (gboolean)theBool);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_float
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1float
  (JNIEnv *env, jclass cls, jobject value, jdouble theDouble)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_value_set_float(value_g, (gdouble)theDouble);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_schema
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1schema
  (JNIEnv *env, jclass cls, jobject value, jobject schema)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	GConfSchema* schema_g = (GConfSchema*)getPointerFromHandle(env, schema);
	gconf_value_set_schema(value_g, schema_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_car
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1car
  (JNIEnv *env, jclass cls, jobject value, jobject car)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	GConfValue* car_g = (GConfValue*)getPointerFromHandle(env, car);
	gconf_value_set_car(value_g, car_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_cdr
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1cdr
  (JNIEnv *env, jclass cls, jobject value, jobject cdr)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	GConfValue* cdr_g = (GConfValue*)getPointerFromHandle(env, cdr);
	gconf_value_set_cdr(value_g, cdr_g);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_list_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1list_1type
  (JNIEnv *env, jclass cls, jobject value, jint type)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_value_set_list_type(value_g, (GConfValueType)type);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_set_list
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1set_1list
  (JNIEnv *env, jclass cls, jobject value, jobjectArray list)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	GConfValueType vt = gconf_value_get_list_type(value_g);
	int i = 0;
	jclass aCls = NULL;
	jobject obj;
	jfieldID fid;
	jmethodID mid;
	jsize len;
	GSList* gslist;
	gpointer data = NULL;
	
	len = (*env)->GetArrayLength(env, list);
	gslist = g_slist_alloc();
	for (i = 0; i < len; i++) {
		obj = (*env)->GetObjectArrayElement(env, list, i);
		if (vt == GCONF_VALUE_STRING) {
			jstring val = (jstring)obj;
			const gchar* v = (*env)->GetStringUTFChars(env, val, NULL);
			data = (gpointer)v;
		}
		else if (vt == GCONF_VALUE_INT) {
			jint val;
			aCls = (*env)->FindClass(env, "java/lang/Integer");
			mid = (*env)->GetMethodID(env, cls, "intValue", "()I");
			if (NULL == mid)
				return;
			val = (*env)->CallIntMethod(env, obj, mid);
			data = (gpointer)&val;
		}
		else if (vt == GCONF_VALUE_FLOAT) {
			jdouble val;
			aCls = (*env)->FindClass(env, "java/lang/Double");
			mid = (*env)->GetMethodID(env, cls, "doubleValue", "()D");
			if (NULL == mid)
				return;
			val = (*env)->CallDoubleMethod(env, obj, mid);
			data = (gpointer)&val;
		}
		else if (vt == GCONF_VALUE_BOOL) {
			jboolean val;
			aCls = (*env)->FindClass(env, "java/lang/Boolean");
			mid = (*env)->GetMethodID(env, cls, "booleanValue", "()Z");
			if (NULL == mid)
				return;
			val = (*env)->CallBooleanMethod(env, obj, mid);
			data = (gpointer)&val;
		}
		else if (vt == GCONF_VALUE_SCHEMA) {
			jobject val;
			aCls = (*env)->FindClass(env, "org/gnu/gconf/ConfSchema");
			fid = (*env)->GetFieldID(env, aCls, "handle", "I");
			if (fid == NULL)
				return;
			val = (jobject)(*env)->GetObjectField(env, obj, fid);
			data = (gpointer)val;
		}
		gslist = g_slist_append(gslist, data);
	}
	gconf_value_set_list(value_g, gslist);
}

/*
 * Class:     org_gnu_gconf_ConfValue
 * Method:    gconf_value_to_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfValue_gconf_1value_1to_1string
  (JNIEnv *env, jclass cls, jobject value)
{
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gchar *str = gconf_value_to_string(value_g);
	return (*env)->NewStringUTF(env, str);
}

#ifdef __cplusplus
}
#endif
