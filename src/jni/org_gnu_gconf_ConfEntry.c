/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gconf/gconf.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gconf_ConfEntry.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_new_nocopy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1new_1nocopy
  (JNIEnv *env, jclass cls, jstring key, jobject val)
{
	GConfValue* val_g =(GConfValue*)getPointerFromHandle(env, val);
	gchar* str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	jobject ret = getStructHandle(env, gconf_entry_new_nocopy(str, val_g), NULL,
			(JGFreeFunc) gconf_entry_unref);
	(*env)->ReleaseStringUTFChars(env, key, str);
	return ret;
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_get_key
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1get_1key
  (JNIEnv *env, jclass cls, jobject entry)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	return (*env)->NewStringUTF(env, gconf_entry_get_key(entry_g));
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_get_value
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1get_1value
  (JNIEnv *env, jclass cls, jobject entry)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	return getStructHandle(env, gconf_entry_get_value(entry_g), 
			(JGCopyFunc) gconf_value_copy, (JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_steal_value
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1steal_1value
  (JNIEnv *env, jclass cls, jobject entry)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	return getStructHandle(env, gconf_entry_steal_value(entry_g), NULL,
			(JGFreeFunc) gconf_entry_free);
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_get_is_default
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1get_1is_1default
  (JNIEnv *env, jclass cls, jobject entry)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	return (jboolean)gconf_entry_get_is_default(entry_g);
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_get_schema_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1get_1schema_1name
  (JNIEnv *env, jclass cls, jobject entry)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	return (*env)->NewStringUTF(env, gconf_entry_get_schema_name(entry_g));
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_set_is_default
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1set_1is_1default
  (JNIEnv *env, jclass cls, jobject entry, jboolean isDefault)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	gconf_entry_set_is_default(entry_g, (gboolean)isDefault);
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_set_schema_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1set_1schema_1name
  (JNIEnv *env, jclass cls, jobject entry, jstring name)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, name, NULL);
	gconf_entry_set_schema_name(entry_g, str);
	(*env)->ReleaseStringUTFChars(env, name, str);
}

/*
 * Class:     org_gnu_gconf_ConfEntry
 * Method:    gconf_entry_set_value_nocopy
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfEntry_gconf_1entry_1set_1value_1nocopy
  (JNIEnv *env, jclass cls, jobject entry, jobject val)
{
	GConfEntry* entry_g = (GConfEntry*)getPointerFromHandle(env, entry);
	GConfValue* val_g = (GConfValue*)getPointerFromHandle(env, val);
	gconf_entry_set_value_nocopy(entry_g, val_g);
}

#ifdef __cplusplus
}
#endif
