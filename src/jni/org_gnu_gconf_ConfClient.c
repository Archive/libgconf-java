/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <jg_jnu.h>
#include <gtk_java.h>

#include "org_gnu_gconf_ConfClient.h"

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1default
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, G_OBJECT(gconf_client_get_default()));
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_for_engine
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1for_1engine
  (JNIEnv *env, jclass cls, jobject engine)
{
	GConfEngine* engine_g = (GConfEngine*)getPointerFromHandle(env, engine);
	return getGObjectHandle(env, G_OBJECT(gconf_client_get_for_engine(engine_g)));
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_add_dir
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1add_1dir
  (JNIEnv *env, jclass cls, jobject client, jstring dir, jint preload)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, dir, NULL);
	GError* err = NULL;
	gconf_client_add_dir(client_g, str, (GConfClientPreloadType)preload, &err);
	(*env)->ReleaseStringUTFChars(env, dir, str);
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_remove_dir
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1remove_1dir
  (JNIEnv *env, jclass cls, jobject client, jstring dir)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, dir, NULL);
	GError* err = NULL;
	gconf_client_remove_dir(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, dir, str);
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

typedef struct 
{
	JNIEnv* env;
	jobject obj;
	jmethodID methodID;
	jstring nameSpace;
} NotifyData;

static void notifyCallback(GConfClient *client, guint cnxn_id, GConfEntry *entry, gpointer data)
{
	NotifyData *nd = (NotifyData*)data;
	jobject entry_g = getStructHandle(nd->env, entry,
			(JGCopyFunc) gconf_entry_copy, (JGFreeFunc) gconf_entry_unref);
	(*nd->env)->CallVoidMethod(nd->env, nd->obj, nd->methodID, entry_g, nd->nameSpace);
}

static void notifyFree(gpointer data)
{
	NotifyData *nd = (NotifyData*)data;
	(*nd->env)->DeleteGlobalRef(nd->env, nd->obj);
	(*nd->env)->DeleteGlobalRef(nd->env, nd->nameSpace);
	g_free((NotifyData*)data);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_notify_add
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1notify_1add
  (JNIEnv *env, jobject obj, jobject client, jstring nameSpace, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	static jmethodID methodID = NULL;
	jclass cls = (*env)->GetObjectClass(env, obj);
	GError* err = NULL;
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, nameSpace, NULL);
	NotifyData *data;
	guint ret;
	if (methodID == NULL) {
		methodID = (*env)->GetMethodID(env, cls, "notifyCallback",
				"(Lorg/gnu/glib/Handle;Ljava/lang/String;)V");
		if (methodID == NULL)
			return -1;
	}
	data = g_new(NotifyData, 1);
	data->env = env;
	data->obj = (*env)->NewGlobalRef(env, obj);
	data->methodID = methodID;
	data->nameSpace = (*env)->NewGlobalRef(env, nameSpace);
	ret = gconf_client_notify_add(client_g, str, notifyCallback, data, notifyFree, &err);
	(*env)->ReleaseStringUTFChars(env, nameSpace, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_notify_remove
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1notify_1remove
  (JNIEnv *env, jclass cls, jobject client, jint cnxn)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gconf_client_notify_remove(client_g, (int)cnxn);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_clear_cache
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1clear_1cache
  (JNIEnv *env, jclass cls, jobject client)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gconf_client_clear_cache(client_g);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_preload
 * Signature: (ILjava/lang/String;I[I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1preload
  (JNIEnv *env, jclass cls, jobject client, jstring dir, jint type, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, dir, NULL);
	GError* err = NULL;
	gconf_client_preload(client_g, str, (GConfClientPreloadType)type, &err);
	(*env)->ReleaseStringUTFChars(env, dir, str);
	if (err != NULL)
		(*env)->SetIntArrayRegion(env, exception, 0, 1, (jint*)err);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject val, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GConfValue* val_g = (GConfValue*)getPointerFromHandle(env, val);
	GError* err = NULL;
	gconf_client_set(client_g, str, val_g, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)
		(*env)->SetIntArrayRegion(env, exception, 0, 1, (jint*)err);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	GConfValue* val = gconf_client_get(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return getStructHandle(env, val, NULL, (JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_without_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1without_1default
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	GConfValue* val = gconf_client_get_without_default(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return getStructHandle(env, val, NULL, (JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_entry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1entry
  (JNIEnv *env, jclass cls, jobject client, jstring key, jstring locale,
  		jboolean schemaDefault, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str1 = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	gchar *str2 = (gchar*)(*env)->GetStringUTFChars(env, locale, NULL);
	GError* err = NULL;
	GConfEntry* val = gconf_client_get_entry(client_g, str1, str2, (gboolean)schemaDefault, &err);
	(*env)->ReleaseStringUTFChars(env, key, str1);
	(*env)->ReleaseStringUTFChars(env, locale, str2);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return getStructHandle(env, val, NULL, (JGFreeFunc) gconf_entry_unref);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_default_from_schema
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1default_1from_1schema
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	GConfValue* val = gconf_client_get_default_from_schema(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return getStructHandle(env, val, NULL, (JGFreeFunc) gconf_value_free);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_unset
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1unset
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gboolean val = gconf_client_unset(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)val;
}

static jobject getGConfEntry(JNIEnv *env, gpointer entry)
{
	return getStructHandle(env, entry, NULL, (JGFreeFunc) gconf_entry_unref);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_all_entries
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1all_1entries
  (JNIEnv *env, jclass cls, jobject client, jstring dir, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, dir, NULL);
	GError* err = NULL;
	GSList* val = gconf_client_all_entries(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, dir, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	if (val == NULL)
		return NULL;
	return getStructHandlesFromGSList(env, val, (GetHandleFunc) getGConfEntry);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_all_dirs
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1all_1dirs
  (JNIEnv *env, jclass cls, jobject client, jstring dir, jobject exception)
{
	int i = 0;
	guint len;
	jclass strCls;
	jobjectArray ar;
	GError* err;
	GSList* val;
	GSList* item;
	GConfClient* client_g;
	gchar *str;
	
	client_g = (GConfClient*)getPointerFromHandle(env, client);
	str = (gchar*)(*env)->GetStringUTFChars(env, dir, NULL);
	err = NULL;
	val = gconf_client_all_dirs(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, dir, str);
	if (val == NULL)
		return NULL;
	len = g_slist_length(val);
	strCls = (*env)->FindClass(env, "java/lang/String");
	if (strCls == NULL)
		return NULL;
	ar = (*env)->NewObjectArray(env, len, strCls, NULL);
	item = val;
	for (i = 0; item != NULL; item = item->next, ++i) {
	 	jstring str = (*env)->NewStringUTF(env, (gchar*)item->data);
		(*env)->SetObjectArrayElement(env, ar, i, str);
	}
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return ar;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_suggest_sync
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1suggest_1sync
  (JNIEnv *env, jclass cls, jobject client)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	GError* err = NULL;
	gconf_client_suggest_sync(client_g, &err);
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_dir_exists
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1dir_1exists
  (JNIEnv *env, jclass cls, jobject client, jstring dir, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, dir, NULL);
	GError* err = NULL;
	gboolean val = gconf_client_dir_exists(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, dir, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)val;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_float
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1float
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gdouble val = gconf_client_get_float(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jdouble)val;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_int
 */
JNIEXPORT jint JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1int
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gint val = gconf_client_get_int(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jint)val;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1string
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gchar* val = gconf_client_get_string(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (*env)->NewStringUTF(env, val);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_bool
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1bool
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gboolean val = gconf_client_get_bool(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)val;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_schema
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1schema
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	GConfSchema* val = gconf_client_get_schema(client_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return getStructHandle(env, val, NULL, (JGFreeFunc) gconf_schema_free);
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_list
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1list
  (JNIEnv *env, jclass cls, jobject client, jstring key, jint type, jobject exception)
{
	GConfClient* client_g;
	GError* err;
	gchar *str;
	GConfValueType vt;
	GSList *item;
	GSList *val;
	jobject schema;
	err = NULL;
	client_g = (GConfClient*)getPointerFromHandle(env, client);
	str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	vt = (GConfValueType)type;
	val = gconf_client_get_list(client_g, str, vt, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	int i = 0;
	jclass aCls = NULL;

	jobjectArray ar;
	if (NULL == val)
		return NULL;
		
	if (vt == GCONF_VALUE_STRING)
		aCls = (*env)->FindClass(env, "java/lang/String");
	else if (vt == GCONF_VALUE_INT)
		aCls = (*env)->FindClass(env, "java/lang/Integer");
	else if (vt == GCONF_VALUE_FLOAT)
		aCls = (*env)->FindClass(env, "java/lang/Double");
	else if (vt == GCONF_VALUE_BOOL)
		aCls = (*env)->FindClass(env, "java/lang/Boolean");
	else if (vt == GCONF_VALUE_SCHEMA)
		aCls = (*env)->FindClass(env, "org/gnu/gconf/ConfSchema");
	if (NULL == aCls)
		return NULL;
	ar = (*env)->NewObjectArray(env, g_slist_length(val), aCls, NULL);
	for (item = val; item != NULL; item = item->next, ++i) {
		if (vt == GCONF_VALUE_STRING) {
			jstring str = (*env)->NewStringUTF(env, item->data);
			(*env)->SetObjectArrayElement(env, ar, i, str);
		}
		else if (vt == GCONF_VALUE_INT) {
			jmethodID mid = (*env)->GetMethodID(env, aCls, "<init>", "(I)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			obj = (*env)->NewObject(env, aCls, mid, (jint)GPOINTER_TO_INT(item->data));
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		}
		else if (vt == GCONF_VALUE_FLOAT) {
			jmethodID mid = (*env)->GetMethodID(env, aCls, "<init>", "(D)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			obj = (*env)->NewObject(env, aCls, mid, (jdouble)(*(gdouble*)(item->data)));
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		}
		else if (vt == GCONF_VALUE_BOOL) {
			jmethodID mid = (*env)->GetMethodID(env, aCls, "<init>", "(Z)V");
			jobject obj;
			if (mid == NULL)
				return NULL;
			obj = (*env)->NewObject(env, aCls, mid, (jboolean)(GPOINTER_TO_INT(item->data) != 0));
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		}
		else if (vt == GCONF_VALUE_SCHEMA) {
		  //			jmethodID mid = (*env)->GetStaticMethodID(env, aCls, "getConfSchema", "(I)V");
			jmethodID mid = (*env)->GetStaticMethodID(env, aCls, "getConfSchema", "(org/gnu/glib/Handle)org/gnu/glib/Handle");
			jobject obj;
			if (mid == NULL)
				return NULL;
			
			schema = getStructHandle(env, item->data,
					(JGCopyFunc) gconf_schema_copy, (JGFreeFunc) gconf_schema_free);
			obj = (*env)->CallStaticObjectMethod(env, aCls, mid, schema);
			(*env)->SetObjectArrayElement(env, ar, i, obj);
		}
	}
	return ar;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_get_pair
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1get_1pair
  (JNIEnv *env, jclass cls, jobject client, jstring key, jint carType, jint
  		cdrType, jintArray car, jintArray cdr, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gpointer carloc = (gpointer)(*env)->GetIntArrayElements(env, car, NULL);
	gpointer cdrloc = (gpointer)(*env)->GetIntArrayElements(env, cdr, NULL);
	gboolean val = gconf_client_get_pair(client_g, str, (GConfValueType)carType, 
			(GConfValueType)cdrType, carloc, cdrloc, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	(*env)->ReleaseIntArrayElements(env, car, (jint*)carloc, 0);
	(*env)->ReleaseIntArrayElements(env, cdr, (jint*)cdrloc, 0);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)val;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_float
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1float
  (JNIEnv *env, jclass cls, jobject client, jstring key, jdouble val, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gboolean ret = gconf_client_set_float(client_g, str, (gdouble)val, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_int
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1int
  (JNIEnv *env, jclass cls, jobject client, jstring key, jint val, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gboolean ret = gconf_client_set_int(client_g, str, (gint)val, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_string
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1string
  (JNIEnv *env, jclass cls, jobject client, jstring key, jstring val, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str1 = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	gchar *str2 = (gchar*)(*env)->GetStringUTFChars(env, val, NULL);
	GError* err = NULL;
	gboolean ret = gconf_client_set_string(client_g, str1, str2, &err);
	(*env)->ReleaseStringUTFChars(env, key, str1);
	(*env)->ReleaseStringUTFChars(env, key, str2);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_bool
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1bool
  (JNIEnv *env, jclass cls, jobject client, jstring key, jboolean val, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gboolean ret = gconf_client_set_bool(client_g, str, (gboolean)val, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_schema
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1schema
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject val, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GConfSchema* val_g = (GConfSchema*)getPointerFromHandle(env, val);
	GError* err = NULL;
	gboolean ret = gconf_client_set_schema(client_g, str, val_g, &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_list
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1list
  (JNIEnv *env, jclass cls, jobject client, jstring key, jint type, jobjectArray list, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	GConfValueType vt = (GConfValueType)type;
	int i = 0;
	jclass aCls = NULL;
	jobject obj;
	jfieldID fid;
	jmethodID mid;
	jsize len;
	GSList* gslist = NULL;
	gpointer data = NULL;
	gboolean ret;
	
	len = (*env)->GetArrayLength(env, list);
	for (i = 0; i < len; i++) {
		obj = (*env)->GetObjectArrayElement(env, list, i);
		if (vt == GCONF_VALUE_STRING) {
			jstring value = (jstring)obj;
			const gchar* v = (*env)->GetStringUTFChars(env, value, NULL);
			data = (gpointer)v;
		}
		else if (vt == GCONF_VALUE_INT) {
			jint value;
			aCls = (*env)->FindClass(env, "java/lang/Integer");
			mid = (*env)->GetMethodID(env, cls, "intValue", "()I");
			if (NULL == mid)
				return (jboolean)JNI_FALSE;
			value = (*env)->CallIntMethod(env, obj, mid);
			data = (gpointer)&value;
		}
		else if (vt == GCONF_VALUE_FLOAT) {
			jdouble value;
			aCls = (*env)->FindClass(env, "java/lang/Double");
			mid = (*env)->GetMethodID(env, cls, "doubleValue", "()D");
			if (NULL == mid)
				return (jboolean)JNI_FALSE;
			value = (*env)->CallDoubleMethod(env, obj, mid);
			data = (gpointer)&value;
		}
		else if (vt == GCONF_VALUE_BOOL) {
			jboolean value;
			aCls = (*env)->FindClass(env, "java/lang/Boolean");
			mid = (*env)->GetMethodID(env, cls, "booleanValue", "()Z");
			if (NULL == mid)
				return (jboolean)JNI_FALSE;
			value = (*env)->CallBooleanMethod(env, obj, mid);
			data = (gpointer)&value;
		}
		else if (vt == GCONF_VALUE_SCHEMA) {
			jobject value;
			aCls = (*env)->FindClass(env, "org/gnu/gconf/ConfSchema");
			fid = (*env)->GetFieldID(env, aCls, "handle", "org/gnu/glib/Handle");
			if (fid == NULL)
				return (jboolean)JNI_FALSE;
			value = (*env)->GetObjectField(env, obj, fid);
			data = (gpointer)getPointerFromHandle(env, value);
		}
		gslist = g_slist_append(gslist, data);
	}
	ret = gconf_client_set_list(client_g, str, vt, gslist, &err);;
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_set_pair
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1set_1pair
  (JNIEnv *env, jclass cls, jobject client, jstring key, jint carType, jint
  		cdrType, jobject car, jobject cdr, jobject exception)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GError* err = NULL;
	gboolean ret = gconf_client_set_pair(client_g, str, (GConfValueType)carType, 
					     (GConfValueType)cdrType,
					     (gconstpointer)getPointerFromHandle(env,car), 
					     (gconstpointer)getPointerFromHandle(env,cdr), 
					     &err);
	(*env)->ReleaseStringUTFChars(env, key, str);
	if (err != NULL)	{
		updateStructHandle(env, exception, err, (JGFreeFunc) g_error_free);
	}
	return (jboolean)ret;
}

/*
 * Class:     org_gnu_gconf_ConfClient
 * Method:    gconf_client_value_changed
 */
JNIEXPORT void JNICALL Java_org_gnu_gconf_ConfClient_gconf_1client_1value_1changed
  (JNIEnv *env, jclass cls, jobject client, jstring key, jobject value)
{
	GConfClient* client_g = (GConfClient*)getPointerFromHandle(env, client);
	gchar *str = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
	GConfValue* value_g = (GConfValue*)getPointerFromHandle(env, value);
	gconf_client_value_changed(client_g, str, value_g);
	(*env)->ReleaseStringUTFChars(env, key, str);
}

#ifdef __cplusplus
}
#endif
