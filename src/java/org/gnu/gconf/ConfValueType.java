/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gconf;

import org.gnu.glib.Enum;

/**
 * Used to indicate the type of a ConfValue.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably may or may not have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gconf.ConfValueType</code>.
 */
public class ConfValueType extends Enum {
    static final private int _INVALID = 0;

    static final public ConfValueType INVALID = new ConfValueType(_INVALID);

    static final private int _STRING = 1;

    static final public ConfValueType STRING = new ConfValueType(_STRING);

    static final private int _INT = 2;

    static final public ConfValueType INT = new ConfValueType(_INT);

    static final private int _FLOAT = 3;

    static final public ConfValueType FLOAT = new ConfValueType(_FLOAT);

    static final private int _BOOL = 4;

    static final public ConfValueType BOOL = new ConfValueType(_BOOL);

    static final private int _SCHEMA = 5;

    static final public ConfValueType SCHEMA = new ConfValueType(_SCHEMA);

    static final private int _LIST = 6;

    static final public ConfValueType LIST = new ConfValueType(_LIST);

    static final private int _PAIR = 7;

    static final public ConfValueType PAIR = new ConfValueType(_PAIR);

    static final private ConfValueType[] theInterned = new ConfValueType[] {
            INVALID, STRING, INT, FLOAT, BOOL, SCHEMA, LIST, PAIR };

    static private java.util.Hashtable theInternedExtras;

    static final private ConfValueType theSacrificialOne = new ConfValueType(0);

    static public ConfValueType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        ConfValueType already = (ConfValueType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new ConfValueType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ConfValueType(int value) {
        value_ = value;
    }

    public ConfValueType or(ConfValueType other) {
        return intern(value_ | other.value_);
    }

    public ConfValueType and(ConfValueType other) {
        return intern(value_ & other.value_);
    }

    public ConfValueType xor(ConfValueType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(ConfValueType other) {
        return (value_ & other.value_) == other.value_;
    }

}
