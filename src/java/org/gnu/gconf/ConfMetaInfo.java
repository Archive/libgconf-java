/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gconf;

import java.util.Date;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

public class ConfMetaInfo extends MemStruct {

    public ConfMetaInfo() {
        super(gconf_meta_info_new());
    }

    public String getSchema() {
        return gconf_meta_info_get_schema(getHandle());
    }

    public String getModUser() {
        return gconf_meta_info_get_mod_user(getHandle());
    }

    public Date getModTime() {
        long time = gconf_meta_info_get_mod_time(getHandle());
        return new Date(time);
    }

    native static final protected Handle gconf_meta_info_new();

    native static final protected void gconf_meta_info_free(Handle info);

    native static final protected String gconf_meta_info_get_schema(Handle info);

    native static final protected String gconf_meta_info_get_mod_user(
            Handle info);

    native static final protected long gconf_meta_info_get_mod_time(Handle info);

    native static final protected void gconf_meta_info_set_schema(Handle info,
            String schema);

    native static final protected void gconf_meta_info_set_mod_user(
            Handle info, String user);

    native static final protected void gconf_meta_info_set_mod_time(
            Handle info, long time);

}
