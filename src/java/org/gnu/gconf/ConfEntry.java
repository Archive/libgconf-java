/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gconf;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Stores an entry for a GConf "directory", including a key-value pair, the name
 * of the schema applicable to this entry, whether the value is a default value,
 * and whether GConf can write a new value at this time.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably may or may not have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gconf.ConfEntry</code>.
 */

public class ConfEntry extends MemStruct {

    /**
     * Construct a new ConfEntry object
     * 
     * @param key
     * @param value
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ConfEntry(String key, ConfValue value) {
        super(gconf_entry_new_nocopy(key, value.getHandle()));
    }

    public ConfEntry(Handle handle) {
        super(handle);
    }

    static ConfEntry getConfEntry(Handle handle) {
        if (handle == null) {
            return null;
        }

        ConfEntry obj = (ConfEntry) MemStruct.getMemStructFromHandle(handle);

        if (obj == null) {
            obj = new ConfEntry(handle);
        }

        return obj;
    }

    /**
     * Return the key field of the entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String getKey() {
        return gconf_entry_get_key(getHandle());
    }

    /**
     * Return the value field of the entry.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ConfValue getValue() {
        Handle hndl = gconf_entry_get_value(getHandle());
        return ConfValue.getConfValue(hndl);
    }

    public void setValue(ConfValue value) {
        gconf_entry_set_value_nocopy(getHandle(), value.getHandle());
    }

    /**
     * Extract the value from this ConfEntry leaving the value set to null.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ConfValue stealValue() {
        Handle hndl = gconf_entry_steal_value(getHandle());
        return ConfValue.getConfValue(hndl);
    }

    public String getSchemaName() {
        return gconf_entry_get_schema_name(getHandle());
    }

    public void setSchemaName(String name) {
        gconf_entry_set_schema_name(getHandle(), name);
    }

    /**
     * Returns if the value in this entry is a default value.
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public boolean isDefault() {
        return gconf_entry_get_is_default(getHandle());
    }

    public void setIsDefault(boolean isDefault) {
        gconf_entry_set_is_default(getHandle(), isDefault);
    }

    native static final protected Handle gconf_entry_new_nocopy(String key,
            Handle value);

    native static final protected String gconf_entry_get_key(Handle entry);

    native static final protected Handle gconf_entry_get_value(Handle entry);

    native static final protected Handle gconf_entry_steal_value(Handle entry);

    native static final protected boolean gconf_entry_get_is_default(
            Handle entry);

    native static final protected String gconf_entry_get_schema_name(
            Handle entry);

    native static final protected void gconf_entry_set_is_default(Handle entry,
            boolean isDefault);

    native static final protected void gconf_entry_set_schema_name(
            Handle entry, String schemaName);

    native static final protected void gconf_entry_set_value_nocopy(
            Handle entry, Handle value);

}
