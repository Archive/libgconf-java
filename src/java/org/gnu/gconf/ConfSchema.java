/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gconf;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably may or may not have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gconf.ConfSchema</code>.
 */
public class ConfSchema extends MemStruct {

    public ConfSchema() {
        super(gconf_schema_new());
    }

    public ConfSchema(Handle handle) {
        super(handle);
    }

    static ConfSchema getConfSchema(Handle handle) {
        if (handle == null) {
            return null;
        }

        ConfSchema obj = (ConfSchema) MemStruct.getMemStructFromHandle(handle);

        if (obj == null) {
            obj = new ConfSchema(handle);
        }

        return obj;
    }

    public String getLocale() {
        return gconf_schema_get_locale(getHandle());
    }

    public void setLocale(String locale) {
        gconf_schema_set_locale(getHandle(), locale);
    }

    public String getShortDescription() {
        return gconf_schema_get_short_desc(getHandle());
    }

    public void setShortDescription(String desc) {
        gconf_schema_set_short_desc(getHandle(), desc);
    }

    public String getLongDescription() {
        return gconf_schema_get_long_desc(getHandle());
    }

    public void setLongDescription(String desc) {
        gconf_schema_set_long_desc(getHandle(), desc);
    }

    public String getOwner() {
        return gconf_schema_get_owner(getHandle());
    }

    public void setOwner(String owner) {
        gconf_schema_set_owner(getHandle(), owner);
    }

    public ConfValueType getType() {
        int hndl = gconf_schema_get_type(getHandle());
        return ConfValueType.intern(hndl);
    }

    public void setType(ConfValueType type) {
        gconf_schema_set_type(getHandle(), type.getValue());
    }

    public ConfValueType getListType() {
        int hndl = gconf_schema_get_list_type(getHandle());
        return ConfValueType.intern(hndl);
    }

    public void setListType(ConfValueType type) {
        gconf_schema_set_list_type(getHandle(), type.getValue());
    }

    public ConfValueType getCarType() {
        int hndl = gconf_schema_get_car_type(getHandle());
        return ConfValueType.intern(hndl);
    }

    public void setCarType(ConfValueType type) {
        gconf_schema_set_car_type(getHandle(), type.getValue());
    }

    public ConfValueType getCdrType() {
        int hndl = gconf_schema_get_cdr_type(getHandle());
        return ConfValueType.intern(hndl);
    }

    public void setCdrType(ConfValueType type) {
        gconf_schema_set_cdr_type(getHandle(), type.getValue());
    }

    public ConfValue getDefaultValue() {
        Handle hndl = gconf_schema_get_default_value(getHandle());
        return ConfValue.getConfValue(hndl);
    }

    public void setDefaultValue(ConfValue value) {
        gconf_schema_set_default_value(getHandle(), value.getHandle());
    }

    native static final protected Handle gconf_schema_new();

    native static final protected void gconf_schema_free(Handle schema);

    native static final protected void gconf_schema_set_type(Handle schema,
            int type);

    native static final protected void gconf_schema_set_list_type(
            Handle schema, int type);

    native static final protected void gconf_schema_set_car_type(Handle schema,
            int type);

    native static final protected void gconf_schema_set_cdr_type(Handle schema,
            int type);

    native static final protected void gconf_schema_set_locale(Handle schema,
            String locale);

    native static final protected void gconf_schema_set_short_desc(
            Handle schema, String desc);

    native static final protected void gconf_schema_set_long_desc(
            Handle schema, String desc);

    native static final protected void gconf_schema_set_owner(Handle schema,
            String owner);

    native static final protected void gconf_schema_set_default_value(
            Handle schema, Handle value);

    native static final protected void gconf_schema_set_default_value_nocopy(
            Handle schema, Handle value);

    native static final protected int gconf_schema_get_type(Handle schema);

    native static final protected int gconf_schema_get_list_type(Handle schema);

    native static final protected int gconf_schema_get_car_type(Handle schema);

    native static final protected int gconf_schema_get_cdr_type(Handle schema);

    native static final protected String gconf_schema_get_locale(Handle schema);

    native static final protected String gconf_schema_get_short_desc(
            Handle schema);

    native static final protected String gconf_schema_get_long_desc(
            Handle schema);

    native static final protected String gconf_schema_get_owner(Handle schema);

    native static final protected Handle gconf_schema_get_default_value(
            Handle schema);

}
