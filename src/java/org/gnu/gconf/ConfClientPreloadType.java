/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gconf;

import org.gnu.glib.Enum;

/**
 * Used to tell ConfCient how to preload one of its directories.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably may or may not have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gconf.ConfClientPreloadType</code>.
 */
public class ConfClientPreloadType extends Enum {
    static final private int _NONE = 0;

    static final public ConfClientPreloadType NONE = new ConfClientPreloadType(
            _NONE);

    static final private int _ONELEVEL = 1;

    static final public ConfClientPreloadType ONELEVEL = new ConfClientPreloadType(
            _ONELEVEL);

    static final private int _RECURSIVE = 2;

    static final public ConfClientPreloadType RECURSIVE = new ConfClientPreloadType(
            _RECURSIVE);

    static final private ConfClientPreloadType[] theInterned = new ConfClientPreloadType[] {
            RECURSIVE, ONELEVEL, RECURSIVE };

    static private java.util.Hashtable theInternedExtras;

    static final private ConfClientPreloadType theSacrificialOne = new ConfClientPreloadType(
            0);

    static public ConfClientPreloadType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        ConfClientPreloadType already = (ConfClientPreloadType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new ConfClientPreloadType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ConfClientPreloadType(int value) {
        value_ = value;
    }

    public ConfClientPreloadType or(ConfClientPreloadType other) {
        return intern(value_ | other.value_);
    }

    public ConfClientPreloadType and(ConfClientPreloadType other) {
        return intern(value_ & other.value_);
    }

    public ConfClientPreloadType xor(ConfClientPreloadType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(ConfClientPreloadType other) {
        return (value_ & other.value_) == other.value_;
    }

}
