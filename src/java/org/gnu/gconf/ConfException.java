/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gconf;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably may or may not have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gconf.ConfException</code>.
 */
public class ConfException extends Exception {
    private int errorCode = 0;

    public static int FAILED = 1;

    public static int NO_SERVER = 2;

    public static int NO_PERMISSION = 3;

    public static int BAD_ADDRESS = 4;

    public static int BAD_KEY = 5;

    public static int PARSE_ERROR = 6;

    public static int CORRUPT = 7;

    public static int TYPE_MISMATCH = 8;

    public static int IS_DIR = 9;

    public static int IS_KEY = 10;

    public static int OVERRIDDEN = 11;

    public static int OAF_ERROR = 12;

    public static int LOCAL_ENGINE = 13;

    public static int LOCK_FAILED = 14;

    public static int NO_WRITABLE_DATABASE = 15;

    public static int IN_SHUTDOWN = 16;

    public ConfException(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
