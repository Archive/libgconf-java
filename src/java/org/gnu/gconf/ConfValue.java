/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gconf;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Represents a dynamically-typed value in GConf.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably may or may not have an equivalent
 *             in java-gnome 4.0; have a look for
 *             <code>org.gnome.gconf.ConfValue</code>.
 */
public class ConfValue extends MemStruct {

    /**
     * Construct a new ConfValue object of the type provided.
     * 
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public ConfValue(ConfValueType type) {
        super(gconf_value_new(type.getValue()));
    }

    public ConfValue(Handle handle) {
        super(handle);
    }

    static ConfValue getConfValue(Handle handle) {
        if (handle == null) {
            return null;
        }

        ConfValue obj = (ConfValue) MemStruct.getMemStructFromHandle(handle);

        if (obj == null) {
            obj = new ConfValue(handle);
        }

        return obj;
    }

    public String getString() {
        return gconf_value_get_string(getHandle());
    }

    public void setString(String theString) {
        gconf_value_set_string(getHandle(), theString);
    }

    public int getInt() {
        return gconf_value_get_int(getHandle());
    }

    public void setInt(int theInt) {
        gconf_value_set_int(getHandle(), theInt);
    }

    public double getDouble() {
        return gconf_value_get_float(getHandle());
    }

    public void setDouble(double theDouble) {
        gconf_value_set_float(getHandle(), theDouble);
    }

    public boolean getBoolean() {
        return gconf_value_get_bool(getHandle());
    }

    public void setBoolean(boolean theBoolean) {
        gconf_value_set_bool(getHandle(), theBoolean);
    }

    public ConfValueType getListType() {
        return ConfValueType.intern(gconf_value_get_list_type(getHandle()));
    }

    public void setListType(ConfValueType type) {
        gconf_value_set_list_type(getHandle(), type.getValue());
    }

    public List getList() {
        List list = new ArrayList();
        Object[] objs = gconf_value_get_list(getHandle());
        for (int i = 0; i < objs.length; i++) {
            list.add(objs[i]);
        }
        return list;
    }

    public void setList(List theList) {
        Object[] objs = new Object[theList.size()];
        for (int i = 0; i < theList.size(); i++) {
            objs[i] = theList.get(i);
        }
        gconf_value_set_list(getHandle(), objs);
    }

    public ConfValue getCar() {
        return ConfValue.getConfValue(gconf_value_get_car(getHandle()));
    }

    public void setCar(ConfValue theCar) {
        gconf_value_set_car(getHandle(), theCar.getHandle());
    }

    public ConfValue getCdr() {
        return ConfValue.getConfValue(gconf_value_get_cdr(getHandle()));
    }

    public void setCdr(ConfValue theCdr) {
        gconf_value_set_cdr(getHandle(), theCdr.getHandle());
    }

    public ConfSchema getSchema() {
        return ConfSchema.getConfSchema(gconf_value_get_schema(getHandle()));
    }

    public void setSchema(ConfSchema theSchema) {
        gconf_value_set_schema(getHandle(), theSchema.getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     * @deprecated Superceeded by java-gnome 4.0; this method may or may not
     *             exist in the new bindings but if it does, it will likely have 
     *             a different name or signature in order that the presented API
     *             is a more algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return gconf_value_to_string(getHandle());
    }

    native static final protected Handle gconf_value_new(int type);

    native static final protected Handle gconf_value_copy(Handle source);

    native static final protected void gconf_value_free(Handle value);

    native static final protected String gconf_value_get_string(Handle value);

    native static final protected int gconf_value_get_int(Handle value);

    native static final protected double gconf_value_get_float(Handle value);

    native static final protected int gconf_value_get_list_type(Handle value);

    native static final protected Object[] gconf_value_get_list(Handle value);

    native static final protected Handle gconf_value_get_car(Handle value);

    native static final protected Handle gconf_value_get_cdr(Handle value);

    native static final protected boolean gconf_value_get_bool(Handle value);

    native static final protected Handle gconf_value_get_schema(Handle value);

    native static final protected void gconf_value_set_int(Handle value,
            int theInt);

    native static final protected void gconf_value_set_string(Handle value,
            String theString);

    native static final protected void gconf_value_set_bool(Handle value,
            boolean theBool);

    native static final protected void gconf_value_set_float(Handle value,
            double theFloat);

    native static final protected void gconf_value_set_schema(Handle value,
            Handle theSchema);

    native static final protected void gconf_value_set_car(Handle value,
            Handle theCar);

    native static final protected void gconf_value_set_cdr(Handle value,
            Handle theCdr);

    native static final protected void gconf_value_set_list_type(Handle value,
            int type);

    native static final protected void gconf_value_set_list(Handle value,
            Object[] theList);

    native static final protected String gconf_value_to_string(Handle value);
}
